//
//  hhhhhApp.swift
//  Shared
//
//  Created by 资源调配中心 uuuuu66699 on 2020/8/24.
//

import SwiftUI

@main
struct hhhhhApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
